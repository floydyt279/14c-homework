#include <iostream>

int main()
{
	using namespace std::literals;

	std::string str1 = "Hello World!";
	std::cout << str1 << '\n';// Result = Hello World!
	

	std::cout << str1.size() << '\n';// Result = 12

	std::cout << str1.front() << str1.back() << std::endl;// Result = H!
}